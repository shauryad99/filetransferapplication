﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Merging_Task
{
    class Program
    {
        static void Main(string[] args)
        {
            string dirPath = @"C:\Users\Shaurya\source\repos\Merging_Task\File3";
            string[] fileAry = Directory.GetFiles(dirPath);

            //to count number of files present in a particular folder
            Console.WriteLine("Total File Count: " + fileAry.Length);

            //to validate whether a given directory exists or not
            if (!Directory.Exists(@"C:\Users\Shaurya\source\repos\Merging_Task"))
            {
                Directory.CreateDirectory(@"C:\Users\Shaurya\source\repos\Merging_Task");
            }

            string fullPath = Path.GetFullPath(fileAry[0]);
            //to get the extension of a file
            string extension = Path.GetExtension(fullPath);
            string outputFileName = "File3" + extension;
            string outputFilePath = @"C:\Users\Shaurya\source\repos\Merging_Task\" + outputFileName;

            //to validate whether a given destination path exists or not
            if (!File.Exists(outputFilePath))
            {
                var myFile = File.Create(outputFilePath);
                myFile.Close();
            }
            //List<string> supportedFiles = new List<string>;         
            using (TextWriter tw = new StreamWriter(outputFilePath, true))
            {
                foreach (string filePath in fileAry)
                {
                    using (TextReader tr = new StreamReader(filePath))
                    {
                        tw.WriteLine(tr.ReadToEnd());
                        tr.Close();
                        tr.Dispose();
                    }
                    Console.WriteLine("File processed : " + filePath);
                }
                tw.Close();
                tw.Dispose();
            }
        }
    }
}
